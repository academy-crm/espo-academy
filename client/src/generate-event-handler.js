define('academy:generate-event-handler', ['action-handler', 'view'], function (Dep, View) {
    return Dep.extend({
        actionGenerateEvents: function (data, e) {
            this.view.createView('dialog', 'academy:views/modals/date-range-picker', {
                id: 0,
                title: 'Hello'
            }, view => {
                view.render();

                this.view.listenToOnce(view, 'create', response => {
                    console.log(response);
                    Espo.Ajax
                        .postRequest('tools/generate/events', {
                            'ids': data.params.ids,
                            'begin': response.begin,
                            'end': response.end
                        })
                        .then(response => {
                            view.close();
                            Espo.Ui.notify("Scheduled generating events for courses!", 'success', 5000);
                        });
                });
            });
        },
        actionGenerateEvent: function (data, e) {
            this.view.createView('dialog', 'academy:views/modals/date-range-picker', {
                id: 0,
                title: 'Hello'
            }, view => {
                view.render();

                this.view.listenToOnce(view, 'create', response => {
                    Espo.Ajax
                        .postRequest('tools/generate/events/' + this.view.model.id, response)
                        .then(response => {
                            view.close();
                            Espo.Ui.notify("Generated " + response.generatedCount + " events", 'success', 5000);
                        });
                });
            });
        },
    });
});