define('academy:generate-invoices-professional-service-handler', ['action-handler'], function (Dep) {

    return Dep.extend({
 
         actionGenerateInvoicesProfessionalService: function (data, e) {
             Espo.Ajax
                 .getRequest('tools/generate/invoicesprofessionalservice/' + this.view.model.id)
                 .then(response => {
                     Espo.Ui.notify("Generated invoices for professional service", 'success', 1000);
                     console.log(response);
                 });
         },
    });
 });