define('academy:generate-invoices-handler', ['action-handler'], function (Dep) {

    return Dep.extend({
 
         actionGenerateInvoices: function (data, e) {
             Espo.Ajax
                 .getRequest('tools/generate/invoices/' + this.view.model.id)
                 .then(response => {
                     Espo.Ui.notify("Generated invoices", 'success', 1000);
                     console.log(response);
                 });
         },
    });
 });