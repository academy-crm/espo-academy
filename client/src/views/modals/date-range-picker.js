define('academy:views/modals/date-range-picker', ['views/modal', 'model'], function (Dep, Model) {
    return Dep.extend({

        className: 'dialog dialog-record',

        // template content can be defined right here or externally
        //templateContent: '',
        template: 'academy:date-range-picker',
        isDraggable: true,
        fitHeight: false,
        header: 'Select period',
        setup: function () {
            this.buttonList = [
                {
                    'name': 'create',
                    'html': this.translate('Create', 'labels', ''),
                    'style': 'success'
                },
                {
                    'name': 'cancel',
                    'label': 'Cancel'
                }
            ];

            let model = new Model();
            model.setDefs({
                'fields': {
                    'startDate': {
                        'type': 'date',
                        'required': true
                    },
                    'endDate': {
                        'type': 'date',
                        'required': true
                    }
                }
            });

            this.createView('startDate', 'views/fields/date', {
                model: model,
                mode: 'edit',
                name: 'startDate',
                label: 'Start',
                el: this.getSelector() + ' .startDate'
            });

            this.createView('endDate', 'views/fields/date', {
                model: model,
                mode: 'edit',
                name: 'endDate',
                el: this.getSelector() + ' .endDate'
            });

            this.model = model;
        },

        actionCreate: function() {
            this.trigger('create', {
                'begin': this.model.get('startDate'),
                'end': this.model.get('endDate')
            });
        }
    })
});