Espo.define('academy:views/fields/inline-checklist', 'academy:views/fields/inline-array', function (Dep) {

    return Dep.extend({

        init: function(){
            Dep.prototype.init.call(this); // call the parent "init()" function
            // if the current view mode is 'list' make the field editable after it has been rendered and when the model has been updated
            if(this.mode==='list') {
                this.listenToOnce(this, 'after:render', this.customInitInlineEdit(), this);       
                this.listenTo(this.model, 'after:save', function () {
                   this.customInitInlineEdit(); 
                }, this);
            }
        },

        type: 'checklist',

        listTemplate: 'fields/array/list',

        detailTemplate: 'fields/checklist/detail',

        editTemplate: 'fields/checklist/edit',

        isInversed: false,

        events: {
        },

        data: function () {
            return _.extend({
                optionDataList: this.getOptionDataList(),
            }, Dep.prototype.data.call(this));
        },

        setup: function () {
            Dep.prototype.setup.call(this);

            this.params.options = this.params.options || [];

            this.isInversed = this.params.isInversed || this.options.isInversed || this.isInversed;
        },

        afterRender: function () {
            if (this.mode == 'search') {
                this.renderSearch();
            }

            if (this.isEditMode()) {
                this.$el.find('input').on('change', function () {
                    this.trigger('change');
                }.bind(this));
            }
        },

        getOptionDataList: function () {
            var valueList = this.model.get(this.name) || [];
            var list = [];

            this.params.options.forEach(function (item) {
                var isChecked = ~valueList.indexOf(item);
                var dataName = 'checklistItem-' + this.name + '-' + item;
                var id = 'checklist-item-' + this.name + '-' + item;

                if (this.isInversed) isChecked = !isChecked;
                list.push({
                    name: item,
                    isChecked: isChecked,
                    dataName: dataName,
                    id: id,
                    label: this.translatedOptions[item] || item,
                });
            }, this);

            return list;
        },

        fetch: function () {
            var list = [];

            this.params.options.forEach(function (item) {
                var $item = this.$el.find('input[data-name="checklistItem-' + this.name + '-' + item + '"]');
                var isChecked = $item.get(0) && $item.get(0).checked;
                if (this.isInversed)
                    isChecked = !isChecked;
                if (isChecked)
                    list.push(item);
            }, this);

            var data = {};
            data[this.name] = list;

            return data;
        },

        validateRequired: function () {
            if (this.isRequired()) {
                var value = this.model.get(this.name);
                if (!value || value.length == 0) {
                    var msg = this.translate('fieldIsRequired', 'messages').replace('{field}', this.getLabelText());
                    this.showValidationMessage(msg, '.checklist-item-container:last-child input');
                    return true;
                }
            }
        },

        validateMaxCount: function () {
            if (this.params.maxCount) {
                var itemList = this.model.get(this.name) || [];
                if (itemList.length > this.params.maxCount) {
                    var msg =
                        this.translate('fieldExceedsMaxCount', 'messages')
                            .replace('{field}', this.getLabelText())
                            .replace('{maxCount}', this.params.maxCount.toString());
                    this.showValidationMessage(msg, '.checklist-item-container:last-child input');
                    return true;
                }
            }
        },
    });
});
