Espo.define('academy:views/fields/inline-base', 'views/fields/base', function (Dep) {

    return Dep.extend({

        customInitInlineEdit: function () { // substitutes initInLineEdit() at base.js
            var $targetEditEl = $('tr[data-id="'+this.model.id+'"] > td[data-name="'+this.name+'"] > a.inline-edit-link');
            // if the edit link already exists, avoid duplicanting
            if($targetEditEl.length != 0) {
                return;
            }            
            var $targetCell = $('tr[data-id="'+this.model.id+'"] > td[data-name="'+this.name+'"]');
            var $editLink = $('<a href="javascript:" class="pull-right inline-edit-link hidden"><span class="fas fa-pencil-alt fa-sm"></span></a>');
            if ($targetCell.length == 0) {
                // if the target cell has not been rendered, wait until it has been rendered to perform the function customInitInlineEdit
                this.listenToOnce(this, 'after:render', this.customInitInlineEdit, this);
                return;
            }
            $targetCell.prepend($editLink);
            $editLink.on('click', function () {
                this.customInlineEdit();
            }.bind(this));
            $targetCell.on('mouseenter', function (e) {
                e.stopPropagation();
                if (this.disabled || this.readOnly) {
                    return;
                }
                $editLink.removeClass('hidden');
            }.bind(this)).on('mouseleave', function (e) {
                e.stopPropagation();
                $editLink.addClass('hidden');
            }.bind(this));
        },

        customInlineEdit: function () { // substitutes inlineEdit() at base.js
            var self = this;
            this.trigger('edit', this);
            this.setMode('edit');
            this.initialAttributes = this.model.getClonedAttributes();
            this.once('after:render', function () {
                this.customAddInlineEditLinks(); // add the edit links "Cancel and Update"
            }, this);
            this._isInlineEditMode = true;
            this.reRender(true);
            this.trigger('inline-edit-on');
        },

        customAddInlineEditLinks: function () { // substitutes addInlineEditLinks() at base.js
            var $targetCell = $('tr[data-id="'+this.model.id+'"] > td[data-name="'+this.name+'"]');
            var $saveLink = $('<a href="javascript:" class="pull-right inline-save-link">' + this.translate('Update') + '</a>');
            var $cancelLink = $('<a href="javascript:" class="pull-right inline-cancel-link">' + this.translate('Cancel') + '</a>');
            $targetCell.prepend($saveLink);
            $targetCell.prepend($cancelLink);
            $targetCell.find('.inline-edit-link').addClass('hidden');            
            $saveLink.click(function () {
                this.customInlineEditSave();
            }.bind(this));            
            $cancelLink.click(function () {
                this.customInlineEditClose();
            }.bind(this));
        },

        customInlineEditSave: function () {
            var data = this.fetch();
            var self = this;
            var model = this.model;
            var prev = this.initialAttributes;
            model.set(data, {silent: true});
            data = model.attributes;
            var attrs = false;
            for (var attr in data) {
                if (_.isEqual(prev[attr], data[attr])) {
                    continue;
                }
                (attrs || (attrs = {}))[attr] =    data[attr];
            }
            if (!attrs) {
                this.customInlineEditClose();
                return;
            }
            if (this.validate()) {
                this.notify('Not valid', 'error');
                model.set(prev, {silent: true});
                return;
            }
            this.notify('Saving...');
            model.save(attrs, {
                success: function () {
                    self.trigger('after:save');
                    model.trigger('after:save');
                    self.notify('Saved', 'success');
                    // if a parent model was passed, refresh the parent model which will refresh the parent view as well
                    if(self.options.parentModel) {
                        self.options.parentModel.fetch();
                    }
                },
                error: function () {
                    self.notify('Error occured', 'error');
                    model.set(prev, {silent: true});
                    self.render()
                },
                patch: true
            });
            this.customInlineEditClose(true);
        },

        customInlineEditClose: function (dontReset) { // substitutes inlineEditClose @ base.js
            this.trigger('inline-edit-off');
            this._isInlineEditMode = false;
            if (this.mode != 'edit') {
                return;
            }
            this.setMode('list');            
            if (!dontReset) {
                this.model.set(this.initialAttributes);
            }
            this.reRender(true);
            this.customInitInlineEdit();
        }

    });
});