define('academy:views/attendance/record/list', ['views/record/list'], function (Dep) {
    return Dep.extend({
        template: 'academy:attendance/list'
    });
});