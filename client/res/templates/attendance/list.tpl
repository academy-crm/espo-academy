<table class="table">
    <thead>
        <tr>
            <th>Schüler</th>
            <th><span style="color:green">Anwesend</span></th>
            <th><span style="color:orange">Entschuldigt</span></th>
            <th><span style="color:red">Gefehlt</span></th>
        </tr>
    </thead>
    <tbody>
        {{#each collection.models}}
        <tr class="list-row">
            <td class="cell">{{this.attributes.pupilName}}</td>
            <td class="cell" width="10"><input type="radio" class="checkbox" value="attendant" name="attendance.{{this.attributes.pupilId}}"/></td>
            <td class="cell" width="10"><input type="radio" class="checkbox" value="apologised" name="attendance.{{this.attributes.pupilId}}"/></td>
            <td class="cell" width="10"><input type="radio" class="checkbox" value="missing" name="attendance.{{this.attributes.pupilId}}"/></td>
        </tr>
        {{/each}}
    </tbody>
</table>
