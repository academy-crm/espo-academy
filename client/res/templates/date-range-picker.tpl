<div class="row">
    <div class="cell col-sm-6 form-group data-name="startDate">
        <label class="control-label" data-name="startDate">Begin</label>
        <div class="startDate" data-name="startDate">{{{startDate}}}</div>
    </div>
    <div class="cell col-sm-6 form-group data-name="endDate">
        <label class="control-label" data-name="endDate">End</label>
        <div class="endDate" data-name="endDate">{{{endDate}}}</div>
    </div>
</div>