<?php

namespace Espo\Modules\Academy\Services;

use Espo\Modules\Academy\Entities\Course as CourseEntity;

class InvoiceProfessionalService extends \Espo\Core\Templates\Services\Base
{
    private static $HOURS_PER_EVENT = 2;
    
    public function generateInvoicesProfessionalServiceForCourse(CourseEntity $course, \DateTime $begin = null, \DateTime $end = null)
    {
        // calculate month and year
        $currDate = new \DateTime();

        $month = $currDate->format('m') - 1;    // previous month
        $year = $currDate->format('Y');
        if ($month == 0)
        {
            $year = $year--;
            $month = 12;
        }
        // calculate date period
        if($begin === null)
            $begin = \DateTime::createFromFormat('Y-m-d',$year.'-'.$month.'-01');    // first day of previous month
        if($end === null)
        {
            $daysInMonth = (int) $begin->format('t');
            $end = \DateTime::createFromFormat('Y-m-d',$year.'-'.$month.'-'.$daysInMonth);     // last day of previous month
        }

        // get Teacher of course
        $teacher = $this->entityManager->getEntityById('Teacher', $course->get('teacherId'));

        $personalId = $teacher->get('personalID');
        $hourlyRate = $teacher->get('hourlyRate');

        $invoiceNumber = $year.'-'.sprintf('%02d', $month).'-'.$personalId;

        // check if invoice already exists
        $invoice = $this->entityManager->getRDBRepository('InvoiceProfessionalService')
        ->where([
            'name' => $invoiceNumber,
        ])->findOne();
    
        if ($invoice == null)
        {
            // create new invoice
            $today = new \DateTime('NOW');
            $invoice = $this->entityManager->createEntity('InvoiceProfessionalService', [
                'name' => $invoiceNumber,
				'invoiceDate' => $today->format('Y-m-d'),
				'invoiceNumber' => $invoiceNumber,
				'status' => 'created',
				'periode' => $begin->format('d.m.').' - '.$end->format('d.m.'),
				'teacherId' => $teacher->get('id'),
            ]);

            if (! $invoice)
            {
                $GLOBALS['log']->error('Invoice not saved!');
            }

            // Items
            $events = $this->entityManager
                ->getRDBRepository('Event')
                ->where([
                    'courseId' => $course->get('id'),
                    'teacherId' => $teacher->get('id'),
                    'dateStart>=' => $begin->format('Y-m-d'),
                    'dateStart<=' => $end->format('Y-m-d'),
                    'status' => 'Held',
                ])
                ->find();

            $pos = 0;
            $amount = 0.0;
            $hours = 0.0;
            foreach($events as $event)
            {
                $pos++;
                $eventDate = $event->get('dateStart');
                if ($eventDate != '')
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s',$eventDate);
                $invoiceItem = $this->entityManager->createEntity('InvoiceItemProfessionalService', [
                    'name'=> $invoiceNumber.'-'.sprintf('%02d', $pos),
                    'position'=> $pos,
                    'invoiceProfessionalServiceId' => $invoice->get('id'),
                    'description' => $date->format('d.m.').' '.$course->get('description'),
                    'quantity' => self::$HOURS_PER_EVENT,
                    'unitPrice' => $hourlyRate,
                    'amount' => self::$HOURS_PER_EVENT * $hourlyRate,
                    'eventId' => $event->get('id'),
                ]);
                
                // compute hours and amount for invoice
                $amount += $hourlyRate * self::$HOURS_PER_EVENT;
                $hours += self::$HOURS_PER_EVENT;
            }
            // update invoice with calculated fields
            $invoice->set([
                'days' => $pos,
                'numberHours' => $hours,
                'netAmount' => $amount,
                'grossAmount' => $amount,
                'tax' => 0,
            ]);
            $this->entityManager->saveEntity($invoice);
        } // $invoice == null

        return true;
    } // public function
}