<?php

namespace Espo\Modules\Academy\Services;

use DateInterval;
use DatePeriod;
use DateTime;

class Course extends \Espo\Core\Templates\Services\Base
{
    private static $DEFAULT_PERIOD = "P1M";

    public function GenerateCourseEvents(string $courseId, DateTime $begin = null, DateTime $end = null)
    {
        // get course entity
        $course = $this->entityManager->getEntityById("Course", $courseId);
        if($course === null || $course === false) {
            $GLOBALS['log']->error('Course ID not given!');
            // @todo propper error handling
            return;
        }

        // calculate date period
        if($begin === null)
            $begin = new DateTime();
        if($end === null)
            $end = (clone $begin)->add(new DateInterval(self::$DEFAULT_PERIOD));

        $weekdays = $this->getWeekdaysOfPeriod($course->get('weekday'), $begin, $end);
        $generatedEvents = [];

        foreach($weekdays as $weekday) 
        {
            $eventNumber = $course->get('name').'-'.$weekday->format('Y-m-d');
            // check if event already exists
            $event = $this->entityManager->getRDBRepository("Event")->where([
                'name' => $eventNumber
            ])->findOne();

            if($event != null)
                continue;

            // create new event
            /** @var \Espo\Modules\Academy\Entities\Event */
            $event = $this->entityManager->createEntity("Event", [
                'name' => $eventNumber,
                'courseId' => $courseId,
                'dateStart' => $weekday->format('Y-m-d').substr($course->get('timeStartsAt'), 10, 9),
                'dateEnd' => $weekday->format('Y-m-d').substr($course->get('timeEndsAt'), 10 ,9),
                'isAllDay' => true,
                'status' => 'Planned',
                'teacherId' => $course->get('teacherId')
            ]);

            $generatedEvents[] = $event;
        }

        return $generatedEvents;
    }

    /**
     * Get all days of week in a period.
     * 
     * @param string $weekday day of week to search (eg: 'monday')
     * @param DateTime $startDate start of period to search in
     * @param DateTime $endDate end of period to search in
     * @return DateTime[] days of week in period
     */
    protected function getWeekdaysOfPeriod(string $weekday, \DateTime $startDate, \DateTime $endDate)
    {
        $interval = new \DateInterval("P1D");
        $period = new \DatePeriod($startDate, $interval, $endDate);

        $weekdays = [];
        foreach($period as $dt) {
            $dayInWeek = strtolower($dt->format('l'));
            
            if($dayInWeek == $weekday) {
                $weekdays[] = clone $dt;
            }
        }

        return $weekdays;
    }

    /**
     * Get days of week of in a month.
     * 
     * @param string $weekday day of week to search (eg: 'monday')
     * @param int $month month to search for weekdays in (defaults to current)
     * @param int $year year of the month (defaults to current)
     * @return DateTime[] days of week in the month
     */
    protected function getWeekdaysOfMonth($weekday, $month = null, $year = null)
    {
        $currDate = new \DateTime();

        if($month == null)
            $month = $currDate->format('m');

        if($year == null)
            $year = $currDate->format('Y');
        
        $startDate = \DateTime::createFromFormat('Y-m-d', $year.'-'.$month.'-01');
        $daysInMonth = (int)$startDate->format('t');
        $endDate = \DateTime::createFromFormat('Y-m-d', $year.'-'.$month.'-'.$daysInMonth);

        return $this->getWeekdaysOfPeriod($weekday, $startDate, $endDate);
    }
}
