<?php

namespace Espo\Modules\Academy\Services;

use DateInterval;
use Espo\Modules\Academy\Entities\Course as CourseEntity;

class InvoiceDirectPayer extends \Espo\Core\Templates\Services\Base
{
    private static $AMOUNT_PER_HOUR = 5;  
    private static $HOURS_PER_EVENT = 2;

    public function generateInvoicesForCourse(CourseEntity $course, \DateTime $begin = null, \DateTime $end = null)
    {
        // calculate month and year
        $currDate = new \DateTime();

        $month = $currDate->format('m') - 1;    // previous month
        $year = $currDate->format('Y');
        if ($month == 0)
        {
            $year = $year--;
            $month = 12;
        }
        // calculate date period
        if($begin === null)
            $begin = \DateTime::createFromFormat('Y-m-d',$year.'-'.$month.'-01');    // first day of previous month
        if($end === null)
        {
            $daysInMonth = (int) $begin->format('t');
            $end = \DateTime::createFromFormat('Y-m-d',$year.'-'.$month.'-'.$daysInMonth);     // last day of previous month
        }

        // !!! Prüfen, ob die Abrechnung nur für einen Kurs oder für alle durchgeführt werden soll
        $pupils = $this->entityManager
            ->getRDBRepository('Course')
            ->getRelation($course, 'pupils')
            ->where([
                'payment' => 'directPayer',
            ])
            ->find();
   

        foreach($pupils as $pupil)
        {
            $personalId = $pupil->get('personalID');
            $invoiceNumber = $year.'-'.sprintf('%02d', $month).'-'.$personalId;

            // check if invoice already exists
            $invoice = $this->entityManager->getRDBRepository('InvoiceDirectPayer')
                ->where([
                    'name' => $invoiceNumber,
                ])->findOne();

            if ($invoice != null)
                    continue;

            // create new invoice
            $today = new \DateTime('NOW');
            $invoice = $this->entityManager->createEntity('InvoiceDirectPayer', [
                'name'=> $invoiceNumber,
                'invoiceDate'=> $today->format('Y-m-d'),
                'invoiceNumber' => $invoiceNumber,
                'teacherId' => $course->get('teacherId'),
                'status' => 'created',
                'periode' => $begin->format('d.m.').' - '.$end->format('d.m.'),
                'pupilId' => $pupil->get('id'),
            ]);

            if (! $invoice)
            {
                $GLOBALS['log']->error('Invoice not saved!');
            }

            $attendances = $this->entityManager
                ->getRDBRepository('Attendance')
                ->distinct()
                ->join('event', 'aliasEvent')
                ->where([
                    'pupilId' => $pupil->get('id'),
                    'aliasEvent.dateStart>=' => $begin->format('Y-m-d'),
                    'aliasEvent.dateStart<=' => $end->format('Y-m-d'),
                    'aliasEvent.status' => 'Held',
                ])
                ->find();

            $pos = 0;
            $amount = 0.0;
            $hours = 0.0;
            foreach($attendances as $attendance)
            {
                $pos++;
                $event = $this->entityManager->getEntity('Event', $attendance->get('eventId'));
                $eventDate = $event->get('dateStart');
                if ($eventDate != '')
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s',$eventDate);
                $invoiceItem = $this->entityManager->createEntity('InvoiceItemDirectPayer', [
                    'name'=> $invoiceNumber.'-'.sprintf('%02d', $pos),
                    'position'=> $pos,-
                    'invoiceDirectPayerId' => $invoice->get('id'),
                    'description' => $date->format('d.m.').' '.$course->get('description'),
                    'quantity' => self::$HOURS_PER_EVENT,
                    'unitPrice' => self::$AMOUNT_PER_HOUR,
                    'amount' => self::$HOURS_PER_EVENT * self::$AMOUNT_PER_HOUR,
                    'eventID' => $attendance->eventID,
                ]);
                
                // compute hours and amount for invoice
                $amount += self::$AMOUNT_PER_HOUR * self::$HOURS_PER_EVENT;
                $hours += self::$HOURS_PER_EVENT;
            }
            // update invoice with calculated fields
            $invoice->set([
                'days' => $pos,
                'numberHours' => $hours,
                'netAmount' => $amount,
                'grossAmount' => $amount,
                'tax' => 0,
            ]);
            $this->entityManager->saveEntity($invoice);
        }
    }
}
