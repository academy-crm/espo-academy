<?php

namespace Espo\Modules\Academy\Entities;

class InvoiceProfessionalService extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'InvoiceProfessionalService';

    protected $entityType = 'InvoiceProfessionalService';
}
