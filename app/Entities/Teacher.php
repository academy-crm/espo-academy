<?php

namespace Espo\Modules\Academy\Entities;

class Teacher extends \Espo\Core\Templates\Entities\Person
{
    public const ENTITY_TYPE = 'Teacher';

    protected $entityType = 'Teacher';
}
