<?php

namespace Espo\Modules\Academy\Entities;

class InvoiceItemProfessionalService extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'InvoiceItemProfessionalService';

    protected $entityType = 'InvoiceItemProfessionalService';
}
