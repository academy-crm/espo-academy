<?php

namespace Espo\Modules\Academy\Entities;

class Pupil extends \Espo\Core\Templates\Entities\Person
{
    public const ENTITY_TYPE = 'Pupil';

    protected $entityType = 'Pupil';
}
