<?php

namespace Espo\Modules\Academy\Entities;

class Attendance extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'Attendance';

    protected $entityType = 'Attendance';
}
