<?php

namespace Espo\Modules\Academy\Entities;

class InvoiceItemDirectPayer extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'InvoiceItemDirectPayer';

    protected $entityType = 'InvoiceItemDirectPayer';
}
