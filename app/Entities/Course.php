<?php

namespace Espo\Modules\Academy\Entities;

class Course extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'Course';

    protected $entityType = 'Course';
}
