<?php

namespace Espo\Modules\Academy\Entities;

class Event extends \Espo\Core\Templates\Entities\Event
{
    public const ENTITY_TYPE = 'Event';

    protected $entityType = 'Event';
}
