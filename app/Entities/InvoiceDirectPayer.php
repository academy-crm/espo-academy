<?php

namespace Espo\Modules\Academy\Entities;

class InvoiceDirectPayer extends \Espo\Core\Templates\Entities\Base
{
    public const ENTITY_TYPE = 'InvoiceDirectPayer';

    protected $entityType = 'InvoiceDirectPayer';
}
