<?php

namespace Espo\Modules\Academy\Jobs\Course;

use DateTime;
use Espo\Core\Job\Job;
use Espo\Core\Job\Job\Data;
use Espo\Modules\Academy\Services\Course as CourseService;

class MassGenerateEvents implements Job
{
    private CourseService $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;    
    }

    public function run(Data $data) : void
    {
        $begin = new DateTime($data->get('begin'));
        $end = new DateTime($data->get('end'));

        foreach($data->get('courseIds') as $courseId)
            $this->courseService->GenerateCourseEvents($courseId, $begin, $end);
    }
}