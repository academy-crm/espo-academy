<?php

namespace Espo\Modules\Academy\Jobs\Course;

use DateTime;
use Espo\Core\Job\Job;
use Espo\Core\Job\Job\Data;
use Espo\Core\Job\JobDataLess;
use Espo\Core\ORM\EntityManager;
use Espo\Modules\Academy\Services\Course as CourseService;

class GenerateEvents implements JobDataLess
{
    private CourseService $courseService;
    private EntityManager $entityManager;

    public function __construct(CourseService $courseService, EntityManager $entityManager)
    {
        $this->courseService = $courseService;
        $this->entityManager = $entityManager;
    }

    public function run() : void
    {
        // get active courses
        $currDate = (new DateTime())->format('Y-m-d');
        $courses = $this->entityManager->getRDBRepository('Course')
            ->where([
                'endsAt>' => $currDate,
                'startsAt<=' => $currDate
            ])->find();
        
        foreach($courses as $course) {
            $this->courseService->GenerateCourseEvents($course->get('id'));
        }
    }
}