<?php

namespace Espo\Modules\Academy\Hooks\Course;

use DateTime;
use Espo\Core\Formula\Functions\JsonGroup\RetrieveType;
use \Espo\Modules\Academy\Services\Course as CourseService;
use \Espo\Modules\Academy\Entities\Course as CourseEntity;
use \Espo\Modules\Academy\Entities\Event as EventEntity;
use Espo\ORM\Repository\Repository;

class CourseAttendencesHook
{
    /** @var \Espo\Core\ORM\EntityManager */
    private $entityManager;

    public function __construct(\Espo\Core\ORM\EntityManager $em)
    {
        $this->entityManager = $em;
    }

    public function afterRelate(CourseEntity $course, array $opts, $data) : void
    {
        if($data['relationName'] == 'pupils')
        {
            // add attendances for pupil to future events
            $futureEvents = $this->getFutureEvents($course);

            foreach($futureEvents as $event)
            {
                $this->entityManager->createEntity('Attendance', [
                    'pupilId' => $data['foreignId'],
                    'eventId' => $event->getId()
                ]);
            }
        }
    }


    public function afterUnrelate(CourseEntity $course, array $opts, $data) : void
    {
        if($data['relationName'] == 'pupils')
        {
            // remove attendances of unrelated pupil for future events
            $futureEvents = $this->getFutureEvents($course);
            $futureAttendances = $this->getAttendancesOfEventsByPupil($futureEvents, $data['foreignId']);
            
            foreach($futureAttendances as $attendance)
                $this->entityManager->removeEntity($attendance);
        }
    }

    public function afterRemove(CourseEntity $course)
    {
        // remove all related events
        $events = $this->entityManager->getRDBRepository('Course')->getRelation($course, 'events')->find();
        
        foreach($events as $event)
            $this->entityManager->removeEntity($event);
    }

    /**
     * @return Repository TEntity of Attendance
     */
    private function getAttendancesOfEventsByPupil($events, $pupilId)
    {
        $eventIds = [];
        foreach($events as $event)
            $eventIds[] = $event->getId();

        return $this->entityManager->getRDBRepository('Attendance')->where([
            'eventId' => $eventIds,
            'pupilId' => $pupilId
        ])->find();
    }

    private function getFutureEvents(CourseEntity $course)
    {
        return $this->entityManager->getRDBRepository('Course')->getRelation($course, 'events')->where([
            'dateStart>=' => (new DateTime())->format('Y-m-d')
        ])->find();
    }
}