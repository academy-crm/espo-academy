<?php

namespace Espo\Modules\Academy\Hooks\Event;

use Espo\Core\ORM\EntityManager;
use \Espo\Modules\Academy\Services\Event as EventService;
use \Espo\Modules\Academy\Entities\Event as EventEntity;

class EventAttendencesHook
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function afterRemove(EventEntity $event, array $opts) : void
    {
        // remove related attendences
        // @todo add config variable to toggle attendences deletion
        $attendences = $this->entityManager
            ->getRDBRepository('Event')
            ->getRelation($event, 'attendances')->find();
            
        foreach($attendences as $attendence)
            $this->entityManager->removeEntity($attendence);
    }

    public function afterSave(EventEntity $event, array $opts): void
    {
        if($event->isNew()) {
            // generate attendences
            $pupils = $this->entityManager
                ->getRDBRepository('Course')
                ->getRelation($event->get('course'), 'pupils')->find();

            foreach($pupils as $pupil) {                
                $this->entityManager->createEntity("Attendance", [
                    'attendance' => "",
                    'eventId' => $event->getId(),
                    'pupilId' => $pupil->getId()
                ]);
            }
        }
    }
}