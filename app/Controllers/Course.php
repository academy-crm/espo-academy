<?php

namespace Espo\Modules\Academy\Controllers;

use DateTime;
use Espo\Core\Api\Request;
use Espo\Core\Api\Response;
use Espo\Core\Job\JobSchedulerFactory;
use Espo\Core\Job\QueueName;
use Espo\Modules\Academy\Jobs\Course\MassGenerateEvents;

class Course extends \Espo\Core\Templates\Controllers\Base
{
    public function getActionGenerateInvoices(Request $request, Response $response)
    {
        $courseId = $request->getRouteParam('course');
        $course = $this->entityManager->getEntityById("Course", $courseId);

        /** @var \Espo\Modules\Academy\Services\InvoiceDirectPayer */
        $invoiceService = $this->recordServiceContainer->get('InvoiceDirectPayer');
        $invoiceService->generateInvoicesForCourse($course);

        return true;
    }    
    
    public function getActionGenerateInvoicesProfessionalService(Request $request, Response $response)
    {
        $courseId = $request->getRouteParam('course');
        $course = $this->entityManager->getEntityById("Course", $courseId);

        /** @var \Espo\Modules\Academy\Services\InvoiceProfessionalService */
        $invoiceService = $this->recordServiceContainer->get('InvoiceProfessionalService');
        $invoiceService->generateInvoicesProfessionalServiceForCourse($course);

        return true;
    }

    public function postActionGenerateEvents(Request $request, Response $response)
    {
        // get entity Course with id => $course
        $courseId = $request->getRouteParam('course');

        $data = $request->getParsedBody();
        
        /** @var \Espo\Modules\Academy\Services\Course */
        $courseService = $this->recordServiceContainer->get('Course');

        $events = $courseService->GenerateCourseEvents($courseId, new DateTime($data->begin), new DateTime($data->end));

        return json_encode([
            'generatedCount' => count($events)
        ]);
    }

    public function postActionMassGenerateEvents(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        /** @var JobSchedulerFactory */
        $jobFactory = $this->injectableFactory->create(JobSchedulerFactory::class);
        $job = $jobFactory->create()
            ->setClassName(MassGenerateEvents::class)
            ->setQueue(QueueName::Q1)
            ->setData([
                'courseIds' => $data->ids,
                'begin' => $data->begin,
                'end' => $data->end,
            ])->schedule();

        $job->set('name', 'Create Events ('.$data->begin.' to '.$data->end.')');
        $this->entityManager->saveEntity($job);

        return json_encode([
            'count' => 0
        ]);
    }
}
